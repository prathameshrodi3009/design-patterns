# Python-Design-Patterns

Design Patterns In Python Language. With Explanations.

### Creational Design Patterns
 * Singleton
 * Factory
 * Abstract Factory
 * Builder 
 
### Structural Design Patters
 * Adapter
 * Composite
 * Proxy
 * Facade
 * Bridge
 * Decorator
 

 